﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WatchAndTrack.Common.ViewModels
{
    public class UsersVM
    {
        public Int32 Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string NormalizedEmail { get; set; }

        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string PasswordHash { get; set; }

        public Int32 PhoneNumber { get; set; }
        public Boolean IsActive { get; set; }

        public Int32 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
