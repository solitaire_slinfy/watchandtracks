﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WatchAndTrack.Common.ViewModels
{
    public class RolesVM
    {
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
