﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WatchAndTrack.Common.ViewModels
{
    public class LoginVM
    {

        [Required]
        [StringLength(255)]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Please Enter Valid Email Address.")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

    }
}
