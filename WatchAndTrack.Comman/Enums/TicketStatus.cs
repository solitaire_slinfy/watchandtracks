﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WatchAndTrack.Common.Enums
{
    public enum TicketStatus
    {
        Open = 1,
        Solved = 2,
        Pending = 3
    };
}
