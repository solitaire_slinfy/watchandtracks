﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WatchAndTrack.Common.Enums
{
    public enum TicketPriority
    {
        None = 0,
        High = 1,
        Medium = 2,
        Low = 3
    };
}
