﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace WatchAndTrack.Common.Helper
{
    public class Encryption
    {
        private static string _key = "a1947exbgc0f15c";

        public static string EncryptToBase64(string _txtToEncrypt, string _key = "E546C8DF278CD5931069B522E695D4F2")
        {
            try
            {
                var key = Encoding.UTF8.GetBytes(_key);

                using (var aesAlg = Aes.Create())
                {
                    using (var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV))
                    {
                        using (var msEncrypt = new MemoryStream())
                        {
                            using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                            using (var swEncrypt = new StreamWriter(csEncrypt))
                            {
                                swEncrypt.Write(_txtToEncrypt);
                            }

                            var iv = aesAlg.IV;
                            var decryptedContent = msEncrypt.ToArray();
                            var result = new byte[iv.Length + decryptedContent.Length];

                            Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                            Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);

                            return Convert.ToBase64String(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return _txtToEncrypt;
            }
            //
        }

        public static string DecryptToBase64(string _txtToDecrypt, string _key = "E546C8DF278CD5931069B522E695D4F2")
        {
            string result;
            try
            {
                var fullCipher = Convert.FromBase64String(_txtToDecrypt);

                var iv = new byte[16];
                var cipher = new byte[16];

                Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
                Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, iv.Length);
                var key = Encoding.UTF8.GetBytes(_key);

                using (var aesAlg = Aes.Create())
                {
                    using (var decryptor = aesAlg.CreateDecryptor(key, iv))
                    {
                        using (var msDecrypt = new MemoryStream(cipher))
                        {
                            using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                            {
                                using (var srDecrypt = new StreamReader(csDecrypt))
                                {
                                    result = srDecrypt.ReadToEnd();
                                }
                            }
                        }
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                return _txtToDecrypt;
            }
        }

    }
}
