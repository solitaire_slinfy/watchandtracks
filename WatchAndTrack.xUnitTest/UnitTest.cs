using System;
using Xunit;

namespace WatchAndTrack.xUnitTest
{
    public class UnitTest
    {
        //go to Test > Windows > Test Explorer > Run All

        //Facts are tests which are always true. They test invariant conditions.

        [Fact]
        public void PassTest()
        {
            Assert.Equal(4, Add(2, 2));
        }

        [Fact]
        public void FailTest()
        {
            Assert.Equal(5, Add(2, 2));
        }

        int Add(int x, int y)
        {
            return x + y;
        }

        //Theories are tests which are only true for a particular set of data.
        [Theory]
        [InlineData(3)]
        [InlineData(5)]
        [InlineData(6)]
        public void TestTheory(int value)
        {
            Assert.True(IsOdd(value));
        }

        bool IsOdd(int value)
        {
            return value % 2 == 1;
        }
    }
}
