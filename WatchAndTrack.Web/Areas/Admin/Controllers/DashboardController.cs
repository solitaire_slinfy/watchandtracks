﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WatchAndTrack.Web.Areas.Admin.Controllers
{
    public class DashboardController : Controller
    {
        [Produces("application/json")]
        [Area("Admin")]
        public IActionResult Index()
        {
            return View();
        }

        [Area("Admin")]
        public IActionResult Login()
        {
            return View();
        }

    }
}