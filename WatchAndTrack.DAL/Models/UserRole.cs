﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WatchAndTrack.DAL.Models
{
    public class UserRole
    {
        [Required]
        [Key]
        public Int32 Id { get; set; }
        
        public Int32 UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User Users { get; set; }

        public Int32 RoleId { get; set; }

        [ForeignKey("RoleId")]
        public virtual Role Roles { get; set; }
    }
}


