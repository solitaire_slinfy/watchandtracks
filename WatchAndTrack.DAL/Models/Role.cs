﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WatchAndTrack.DAL.Models
{
    public class Role
    {
        [Required]
        [Key]
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
