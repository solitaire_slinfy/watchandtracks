﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WatchAndTrack.DAL.Models
{
    public class Ticket
    {
        [Required]
        [Key]
        public Int32 Id { get; set; }

        public string Subject { get; set; }
        public string Description { get; set; }
        public Int32? Priority { get; set; }

        public string Answer { get; set; }
        public Boolean IsActive { get; set; }
        public Int32? Status { get; set; }

        public Int32 CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }

        public Int32? ParentId { get; set; }

        [ForeignKey("ParentId")]
        public virtual Ticket Parent { get; set; }

        public virtual ICollection<Ticket> Children { get; set; }
    }
}
